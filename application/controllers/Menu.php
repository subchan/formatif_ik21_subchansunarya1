<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class menu extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Menu_model");
		
	}
	
	public function index()
	
	{
		$this->listmenu();
	}
	
	public function listmenu()
	
	{
		$data['data_menu'] = $this->Menu_model->tampilDataMenu();
		$this->load->view('homeMenu', $data);
	}
	
	
		public function input()
	{
		$data['data_menu'] = $this->Menu_model->tampilDataMenu();
		
		if (!empty($_REQUEST)) {
			$m_menu = $this->Menu_model;
			$m_menu->save();
			redirect("Menu/index", "refresh"); 
		}
		
		
		$this->load->view('InputMenu', $data);
		
		
	}
	
	   
	   public function Editmenu($kode_menu)
	{
		$data['detail_menu'] = $this->Menu_model->detail($kode_menu);
		
		if (!empty($_REQUEST)) {
			$m_menu = $this->Menu_model;
			$m_menu->update($kode_menu);
			redirect("Menu/index", "refresh"); 
		}
		
		
		$this->load->view('editmenu', $data);
	}
	
	public function deleteMenu($kode_menu)
	{
		$m_menu = $this->Menu_model;
		$m_menu->delete($kode_menu);
		redirect("Menu/index", "refresh");
		
	}
	
	
	   
}

