<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Karyawann extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Karyawan_Model");
		
	}
	
	public function index()
	
	{
		$this->listKaryawan();
	}
	
	public function listKaryawan()
	
	{
		$data['data_karyawan'] = $this->Karyawan_Model->tampilDataKaryawan();
		$this->load->view('homeKaryawan', $data);
	}
	
	
		public function input()
	{
		$data['data_karyawan'] = $this->Karyawan_Model->tampilDataKaryawan();
		
		if (!empty($_REQUEST)) {
			$m_karyawan = $this->Karyawan_Model;
			$m_karyawan->save();
			redirect("Karyawann/index", "refresh"); 
		}
		
		
		$this->load->view('InputKaryawan', $data);
		
		
	}
	
	   public function detailkaryawan($nik)
	   {
			$data['detail_karyawan']	= $this->Karyawan_Model->detail($nik);
			$this->load->view('detail_karyawan', $data);   
	   }
	   
	   public function Editkaryawan($nik)
	{
		$data['detail_karyawan'] = $this->Karyawan_Model->detail($nik);
		
		if (!empty($_REQUEST)) {
			$m_karyawan = $this->Karyawan_Model;
			$m_karyawan->update($nik);
			redirect("Karyawann/index", "refresh"); 
		}
		
		
		$this->load->view('Editkaryawan', $data);
	}
	
	public function deletekaryawan($nik)
	{
		$m_karyawan = $this->Karyawan_Model;
		$m_karyawan->delete($nik);
		redirect("Karyawann/index", "refresh");
		
	}
	
	
	   
}

