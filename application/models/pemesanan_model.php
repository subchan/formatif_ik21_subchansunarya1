<?php defined ('BASEPATH') OR exit ('no direct script access allowed');

class pemesanan_model extends CI_model
{
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Menu_model");
		
	}
	
	//panggil nama table
	private $_table = "transaksi_pemesanan";
	
	public function tampilDataPemesanan()
	{
		//seperti : select * from <nama_table> "cara 1"
		return $this->db->get($this->_table)->result();
	}
	
	public function save($id)
	{
		
		$pegawai = $this->input->post('nik');
		$nama = $this->input->post('nama');
		$kode_menu = $this->input->post('kode_menu');
		$qty = $this->input->post('qty');
		
		//buat cari menu di file menu model
			$hargamenu = $this->Menu_model->cariHargamenu($kode_menu);
		
		$data['id_pemesanan']	= $id;
		$data['nik']	= $pegawai;
		$data['tgl_pemesanan']	= date ('y-m-d');
		$data['nama_pelanggan'] = $nama ;
		$data['kode_menu']	= $kode_menu;
		$data['qty']	= $qty;
		$data['total_harga']	= $qty * $hargamenu;

		$this->db->insert($this->_table, $data);
	}
	
	public function delete($id_pemesanan)
	{
		$this->db->where('id_pemesanan', $id_pemesanan);
		$this->db->delete($this->_table);
		
	}
	
	
	
	
}


